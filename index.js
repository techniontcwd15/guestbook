const mongoose = require('mongoose'); // import mongoose
const express = require('express'); // import express
const bodyParser = require('body-parser'); // import body parser
const Comment = require('./model/comment');
//connection to the db guestBookDB
mongoose.connect('mongodb://localhost/guestBookDB');

// crate express application
const app = express();

app.use(express.static('guest-book-client/dist'));
app.use(bodyParser.json());

// get all comments route
app.get('/api/comments', (req, res) => {
  Comment.find((err, comments) => { // find all comments
    if (err) {
      res.writeHead(500);
      res.end();
    } else {
      res.json(comments);
    }
  });
});

// add new comment
app.post('/api/comments', (req, res) => {
  // create new comment
  const comment = new Comment(req.body);
  comment.save((err) => { // save comment
    if (err) {
      res.writeHead(500);
      res.end();
    } else {
      res.writeHead(204);
      res.end();
    }
  });
});

// get all comments with name
app.get('/api/comments/:name', (req, res) => {
  Comment.find({name: req.params.name}, (err, comments) => { // find all comments
    if (err) {
      res.writeHead(500);
      res.end();
    } else {
      res.json(comments);
    }
  });
});

// listen to port 3000
app.listen(3000);
