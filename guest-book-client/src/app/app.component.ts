import { Component, OnInit } from '@angular/core';
import { RestService } from './rest.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [RestService]
})
export class AppComponent implements OnInit {
  private comments = [];

  constructor(private rest:RestService) { }

  addComment(name:string, content:string) {
    this.rest.addComment(name, content).subscribe(() => {
      this.getComments();
    });
  }

  getComments() {
    this.rest.getComments().subscribe((resComments) => {
      this.comments = resComments;
    });
  }

  ngOnInit() {
    this.getComments();
  }
}
