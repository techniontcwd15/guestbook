const mongoose = require('mongoose'); // import mongoose

//create comment model with schema (comments collection will create automatically)
const CommentSchema = {
  name: String,
  email: String,
  content: String,
  timestamp: Date
}
module.exports = mongoose.model('Comment', CommentSchema);
